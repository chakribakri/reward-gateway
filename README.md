Code Test project for Reward Gateway

## Installation on linux

Copy `.env.dist` as `.env` and update the details in there. Then run `composer install` to install all required libraries.

Once all the packages/libraries have been installed, run the webserver by running this command:
```
bin/console server:start
```

The project can be then accessed from http://localhost:8000 url.
