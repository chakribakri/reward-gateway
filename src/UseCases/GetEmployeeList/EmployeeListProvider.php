<?php

namespace App\UseCases\GetEmployeeList;

interface EmployeeListProvider
{
    public function getList(): array;
}
