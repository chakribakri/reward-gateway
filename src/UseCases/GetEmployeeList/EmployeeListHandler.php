<?php

namespace App\UseCases\GetEmployeeList;

class EmployeeListHandler
{
    /**
     * @var EmployeeListProvider
     */
    private $provider;

    public function __construct(
        EmployeeListProvider $provider
    )
    {
        $this->provider = $provider;
    }

    public function handle() {

        $data = $this->provider->getList();

        return $data;
    }
}
