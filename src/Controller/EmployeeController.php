<?php

namespace App\Controller;

use App\UseCases\GetEmployeeList\EmployeeListHandler;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/", name="employee")
 */
class EmployeeController extends AbstractController
{
    /**
     * @Route("/", name="list")
     *
     * @param EmployeeListHandler $handler
     * @return Response
     */
    public function getEmployeeList(EmployeeListHandler $handler): Response
    {
        try {
            $data = $handler->handle();
        }catch (ClientException $exception) {
            return $this->render('error/error.html.twig', ['data' => 'client exception']);
        } catch (ServerException $exception) {
            return $this->render('error/error.html.twig', ['data' => 'server exception']);
        }

        return $this->render('employee/list.html.twig', ['data' => $data]);
    }
}
