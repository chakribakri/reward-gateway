<?php

namespace App\Services;

use App\Exception\DomainException;
use App\UseCases\GetEmployeeList\EmployeeListProvider;
use GuzzleHttp\ClientInterface;

class RewardGatewayApi implements EmployeeListProvider
{
    /**
     * @var ClientInterface
     */
    private $client;

    public function __construct(
        ClientInterface $client
    ) {
        $this->client = $client;
    }

    /**
     * @return array
     *
     * @throws DomainException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getList(): array
    {
        $response = $this->client->request('GET', 'http://hiring.rewardgateway.net/list');

        if (200 !== $response->getStatusCode() && 'OK' !== $response->getReasonPhrase()) {
            throw new DomainException(sprintf('Failed to get employee list from the API. "%s", "%s"',$response->getStatusCode(), $response->getReasonPhrase()));
        }

        return json_decode($response->getBody()->getContents(), true);
    }

}
