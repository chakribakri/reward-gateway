<?php

namespace App\Services;

use JMS\Serializer\Annotation as Serializer;

class EmployeeDto
{
    /**
     * @Serializer\Type("string")
     *
     * @var string
     */
    public $uuid;

    /**
     * @Serializer\Type("string")
     *
     * @var string
     */
    public $company;

    /**
     * @Serializer\Type("string")
     *
     * @var string
     */
    public $bio;

    /**
     * @Serializer\Type("string")
     *
     * @var string
     */
    public $name;

    /**
     * @Serializer\Type("string")
     *
     * @var string
     */
    public $title;

    /**
     * @Serializer\Type("string")
     *
     * @var
     */
    public $avatar;

    public function normalise()
    {
        return [
            'uuid' => $this->uuid,
            'title' => $this->title,
            'name' => $this->name,
            'company' => $this->company,
            'bio' => $this->bio,
            'avatar' => $this->avatar,
        ];
    }
}
