<?php

namespace App\Services;

use JMS\Serializer\Annotation as Serializer;

class EmployeeResponse
{
    /**
     * @Serializer\Type("array<App\Services\EmployeeDto>")
     * @var EmployeeDto[]|array
     */
    public $employeeCollection;

    public function normalise()
    {
        return array_filter($this->employeeCollection, function(EmployeeDto $employee) {
            return $employee->normalise();
        });
    }
}
