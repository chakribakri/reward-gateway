<?php

namespace App\Tests\Unit\UseCases;

use App\Services\RewardGatewayApi;
use App\Tests\Fixtures\RewardGatewayApiResponseFixtures;
use App\UseCases\GetEmployeeList\EmployeeListHandler;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\TestCase;
use Prophecy\Prophecy\ObjectProphecy;

class EmployeeListTest extends TestCase
{
    /**
     * @var EmployeeListHandler
     */
    private $handler;

    /**
     * @var ObjectProphecy
     */
    private $clientProphet;

    protected function setUp()
    {
        parent::setUp();
        $this->clientProphet = $this->prophesize(Client::class);
        $this->handler = new EmployeeListHandler(new RewardGatewayApi($this->clientProphet->reveal()));
    }

    protected function tearDown()
    {
        $this->clientProphet = null;
        $this->handler = null;
        parent::tearDown();
    }

    /**
     * @test
     */
    public function returns_data_from_the_employee_provider_service(): void
    {
        $successResponnse = new Response(200, [], RewardGatewayApiResponseFixtures::successResponse());
        $this->clientProphet->request('GET', 'http://hiring.rewardgateway.net/list')
            ->shouldBeCalled()
            ->willReturn($successResponnse);

        $response = $this->handler->handle();
        Assert::assertArrayHasKey('uuid', $response[0]);
        Assert::assertArrayHasKey('title', $response[0]);
        Assert::assertArrayHasKey('name', $response[0]);
        Assert::assertArrayHasKey('company', $response[0]);
        Assert::assertArrayHasKey('bio', $response[0]);
        Assert::assertArrayHasKey('avatar', $response[0]);
    }
}
