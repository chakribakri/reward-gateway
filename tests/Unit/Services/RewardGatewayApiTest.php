<?php

namespace App\Tests\Unit\Services;

use App\Exception\DomainException;
use App\Services\RewardGatewayApi;
use App\Tests\Fixtures\RewardGatewayApiResponseFixtures;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\TestCase;
use Prophecy\Prophecy\ObjectProphecy;

class RewardGatewayApiTest extends TestCase
{
    /**
     * @var RewardGatewayApi
     */
    private $client;

    /**
     * @var ObjectProphecy
     */
    private $clientProphet;

    protected function setUp()
    {
        parent::setUp();
        $this->clientProphet = $this->prophesize(Client::class);
        $this->client = new RewardGatewayApi($this->clientProphet->reveal());
    }

    /**
     * @test
     */
    public function returns_response_with_valid_request(): void
    {
        $successResponse = new Response(
            200,
            ['Content-Type' => 'application/json'],
            RewardGatewayApiResponseFixtures::successResponse()
        );
        $this->clientProphet->request('GET', 'http://hiring.rewardgateway.net/list')
            ->shouldBeCalled()
            ->willReturn($successResponse);
        Assert::assertInstanceOf(Response::class, $successResponse);
        $list = $this->client->getList();
        
        Assert::assertArrayHasKey('uuid', $list[0]);
        Assert::assertArrayHasKey('name', $list[0]);
        Assert::assertArrayHasKey('title', $list[0]);
        Assert::assertArrayHasKey('company', $list[0]);
        Assert::assertArrayHasKey('bio', $list[0]);
        Assert::assertArrayHasKey('avatar', $list[0]);
    }

    /**
     * @test
     *
     * @throws DomainException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function throws_exception_when_failed_to_connect_to_api(): void
    {
        $this->expectException(DomainException::class);

        $errorResponse = new Response(401, ['Content-Type' => 'application/json']);
        $this->clientProphet->request('GET', 'http://hiring.rewardgateway.net/list')
            ->shouldBeCalled()
            ->willReturn($errorResponse);

        $this->client->getList();
    }
}
