<?php

namespace App\Tests\Integration;

use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class EmployeeListTest extends WebTestCase
{
    /**
     * @var Client
     */
    private $client;

    protected function setUp()
    {
        parent::setUp();
        $this->client = static::createClient();
    }

    protected function tearDown()
    {
        $this->client = null;
        parent::tearDown();
    }

    /**
     * @test
     */
    public function returns_list_of_employees_with_valid_request(): void
    {
        $crawler = $this->client->request('GET', '/');
        $this->assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
        $h1 = $crawler->filter('h1:contains("List of employee")')->count();
        $this->assertEquals(1, $h1);
    }
}
